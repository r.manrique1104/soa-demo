package ws;


import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Demo {

    @WebMethod
    String getMensaje();

    @WebMethod
    String getMensaje2(String name);

}
