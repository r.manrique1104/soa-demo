package ws.impl;

import ws.Demo;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;


@WebService(endpointInterface = "ws.Demo")
public class DemoImpl implements Demo {

    @Resource
    WebServiceContext wsctx;

    @Override
    public String getMensaje() {
        return "hola mundo...";
    }

    @Override
    public String getMensaje2(String name) {
        return "hola deunevo " + name;
    }
}
