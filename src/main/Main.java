package main;

import javax.xml.ws.*;
import ws.impl.DemoImpl;

public class Main {
    public static void main(String[] args ){
        try {
            Endpoint.publish("http://localhost:9991/ws/demo", new DemoImpl());
            System.out.println("corriendo..");
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
